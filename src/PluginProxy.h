//
//  PluginInstance.h
//  EzPlugin
//
//  Created by Oleg on 19.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __EzPlugin__PluginInstance__
#define __EzPlugin__PluginInstance__

#include "npapi/npapi.h"
#include "npapi/npfunctions.h"

void        PluginProcessInit();
void        PluginProcessFree();
void*       CreatePluginInstance    (NPNetscapeFuncs *browser_func,NPP npp);
void        DeletePluginInstance    (void *value);
void*       GetCALayer              (void *value);
NPClass*    GetNPClass              (void *value);
void        ProcessCocoaEvent       (void *value,const NPCocoaEvent *event);

#endif /* defined(__EzPlugin__PluginInstance__) */
