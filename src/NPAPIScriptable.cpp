#include "NPAPIScriptable.h"

using namespace CoreObjectLib;
using namespace PluginLib;
using namespace std;

template<typename T>
class NPAPIUniquePtr
{
public:
	NPAPIUniquePtr(NPNetscapeFuncs* npnfuncs,T *ptr = nullptr)
		: _npnfuncs(npnfuncs),_ptr(ptr)
	{
	}
	virtual ~NPAPIUniquePtr()
	{
		if (_ptr != nullptr)
		{
			_npnfuncs->memfree(_ptr);
		}
	}

	void Set(T *ptr)
	{
		_ptr = ptr;
	}

	T* operator()()
	{
		return _ptr;
	}

	T* operator->()
	{
		return _ptr;
	}
private:
	T *_ptr;
	NPNetscapeFuncs* _npnfuncs;
};

NPAPIScriptable::NPAPIScriptable(NPP instance)
: _npp(instance)
{
	PluginInfo *info;

	info = (PluginInfo*)GetNPP()->pdata;
	_plugin = (JSApiBase*)info->plugin;
	_core = info->core;
}

NPAPIScriptable::~NPAPIScriptable()
{
	
}

NPObject* NPAPIScriptable::Allocate(NPP instance, NPClass* npclass) 
{
	 return (NPObject*)(new NPAPIScriptable(instance));
}

void NPAPIScriptable::Deallocate(NPObject* obj) 
{
  delete (NPAPIScriptable*)obj;
}

bool NPAPIScriptable::HasMethod(NPObject* obj, NPIdentifier np_name)
{
	NPUTF8 *name;
	bool ret_val;
	NPAPIScriptable *scriptable;

	scriptable = (NPAPIScriptable*)obj;

	name = npnfuncs->utf8fromidentifier(np_name);
	ret_val = scriptable->_plugin->HasJSMethod(name);
	npnfuncs->memfree(name);

	return ret_val;
}

bool NPAPIScriptable::InvokeDefault(NPObject* obj, const NPVariant* args,
                            uint32_t argCount, NPVariant* result)
{
	return true;
}

bool NPAPIScriptable::Invoke(NPObject* obj, NPIdentifier method_name,
                     const NPVariant* args, uint32_t arg_count,
                     NPVariant* result)
{
	char* name;
	bool ret_val = false;
	NPAPIScriptable *scriptable;
	VariantFunction function;
	vector<Variant> vec_args;
	Variant arg,return_variant;
	string string_val;
	NPAPIUniquePtr<char> uname(npnfuncs,npnfuncs->utf8fromidentifier(method_name));

	name = uname();

	scriptable = (NPAPIScriptable*)obj;

	RETURN_IF_FALSE(name);
	RETURN_IF_FALSE(scriptable->_plugin->GetMethod(name,&function));
	
	for (auto i = 0; i < arg_count; ++i)
	{
		switch(args[i].type)
		{
		case NPVariantType_Void:
			continue;
		break;
		case NPVariantType_Null:
			arg.SetType(VariantType::Unknown);
		break;
		case NPVariantType_Bool:
			arg = args[i].value.boolValue;
		break;
		case NPVariantType_Int32:
			arg = args[i].value.intValue;
		break;
		case NPVariantType_Double:
			arg = args[i].value.doubleValue;
		break;
		case NPVariantType_String:
			string_val.assign(args[i].value.stringValue.UTF8Characters,args[i].value.stringValue.UTF8Length);
			arg = string_val;
		break;
		case NPVariantType_Object:
			return false;
		break;
		}
		vec_args.push_back(arg);
	} 
	return_variant = function(vec_args);
	switch (return_variant.GetType())
	{
		case VariantType::SignedInt8:
		case VariantType::SignedInt16:
		case VariantType::SignedInt32:
		case VariantType::SignedInt64:
		case VariantType::UnsignedInt8:
		case VariantType::UnsignedInt16:
		case VariantType::UnsignedInt32:
		case VariantType::UnsignedInt64:
			INT32_TO_NPVARIANT(return_variant.AsInt32(),*result);
		break;
		case VariantType::Double:
		case VariantType::Float:
			DOUBLE_TO_NPVARIANT(return_variant.AsDouble(),*result);
		break;
		case VariantType::Bool:
			BOOLEAN_TO_NPVARIANT(return_variant.AsBool(),*result);
		break;
		case VariantType::String:
			STRINGZ_TO_NPVARIANT(return_variant.AsChar(),*result);
		break;
		default:
			return false;
	}
	return ret_val;
}

bool NPAPIScriptable::HasProperty(NPObject* obj, NPIdentifier np_name)
{
	NPUTF8 *name;
	bool ret_val;
	NPAPIScriptable *scriptable;

	scriptable = (NPAPIScriptable*)obj;

	name = npnfuncs->utf8fromidentifier(np_name);
	ret_val = scriptable->_plugin->HasJSProperty(name) || scriptable->_plugin->HasJSEvent(name);
	npnfuncs->memfree(name);
	return ret_val;
}

bool NPAPIScriptable::GetProperty(NPObject* obj, NPIdentifier property_name,
                          NPVariant* result) 
{
	NPAPIUniquePtr<char> uname(npnfuncs,npnfuncs->utf8fromidentifier(property_name));
	NPAPIScriptable *scriptable;
	Variant val;
	NPObject *np_object;
	
	scriptable = (NPAPIScriptable*)obj;

	RETURN_IF_FALSE(uname());
	RETURN_IF_FALSE(scriptable->_plugin->HasJSProperty(uname()) || scriptable->_plugin->HasJSEvent(uname()));

	if (scriptable->_plugin->GetJSProperty(uname(),&val))
	{
		switch(val.GetType())
		{
		case VariantType::SignedInt8:
		case VariantType::SignedInt16:
		case VariantType::SignedInt32:
		case VariantType::SignedInt64:
		case VariantType::UnsignedInt8:
		case VariantType::UnsignedInt16:
		case VariantType::UnsignedInt32:
		case VariantType::UnsignedInt64:
			INT32_TO_NPVARIANT(val.AsInt32(),*result);
		break;
		case VariantType::Double:
		case VariantType::Float:
			DOUBLE_TO_NPVARIANT(val.AsDouble(),*result);
		break;
		case VariantType::Bool:
			BOOLEAN_TO_NPVARIANT(val.AsBool(),*result);
		break;
		case VariantType::String:
			STRINGZ_TO_NPVARIANT(val.AsChar(),*result);
		break;
		default:
			return false;
		}
	}
	else if (scriptable->_plugin->GetJSEvent(uname(),(void**)&np_object))
		OBJECT_TO_NPVARIANT(np_object,*result);
	else
	{
		fprintf(stderr,"FATAL: unknown internal error. Couldn't find neither event, nor property");
		abort();
	}
	
	return true;
}

bool NPAPIScriptable::SetProperty(NPObject *obj, NPIdentifier property_name, const NPVariant *value)
{
	NPAPIUniquePtr<char> uname(npnfuncs,npnfuncs->utf8fromidentifier(property_name));
	NPAPIScriptable *scriptable;
	Variant val;
	
	scriptable = (NPAPIScriptable*)obj;

	RETURN_IF_FALSE(uname());
	RETURN_IF_FALSE(scriptable->_plugin->HasJSProperty(uname()) || scriptable->_plugin->HasJSEvent(uname()));

	if (scriptable->_plugin->HasJSProperty(uname()))
	{
		switch(value->type)
		{
		case NPVariantType_Int32:
			val = NPVARIANT_TO_INT32(*value);
			scriptable->_plugin->SetJSProperty(uname(),val);
		break;
		case NPVariantType_Double:
			val = NPVARIANT_TO_DOUBLE(*value);
			scriptable->_plugin->SetJSProperty(uname(),val);
		break;
		case NPVariantType_Bool:
			val = NPVARIANT_TO_BOOLEAN(*value);
			scriptable->_plugin->SetJSProperty(uname(),val);
		break;
		case NPVariantType_String:
			val = string(NPVARIANT_TO_STRING(*value).UTF8Characters,NPVARIANT_TO_STRING(*value).UTF8Length);
			scriptable->_plugin->SetJSProperty(uname(),val);
		break;
		default:
			return false;
		}
	}
	else if (scriptable->_plugin->HasJSEvent(uname()))
	{
		switch(value->type)
		{
		case NPVariantType_Object:
			npnfuncs->retainobject(NPVARIANT_TO_OBJECT(*value));
			scriptable->_plugin->SetJSEvent(uname(),(void*)NPVARIANT_TO_OBJECT(*value));
		break;
		default:
			return false;
		}
	}
	else return false;
	return true;
}

bool NPAPIScriptable::RemoveProperty(NPObject *obj, NPIdentifier property_name)
{
	NPAPIUniquePtr<char> uname(npnfuncs,npnfuncs->utf8fromidentifier(property_name));
	NPAPIScriptable *scriptable;
	Variant val;
	NPObject *np_object;

	scriptable = (NPAPIScriptable*)obj;

	if (scriptable->_plugin->HasJSProperty(uname()))
	{
		scriptable->_plugin->SetJSProperty(uname(),Variant());
		return true;
	}
	else if (scriptable->_plugin->HasJSEvent(uname()))
	{
		scriptable->_plugin->GetJSEvent(uname(),(void**)&np_object);
		if (np_object != nullptr)
			npnfuncs->releaseobject(np_object);
		scriptable->_plugin->SetJSEvent(uname(),nullptr);
		return true;
	}
	
	return false;
}