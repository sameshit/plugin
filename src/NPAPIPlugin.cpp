#include "NPAPIPlugin.h"

using namespace PluginLib;
using namespace CoreObjectLib;
using namespace std;

static NPClass plugin_ref_obj = 
{
  NP_CLASS_STRUCT_VERSION,
  NPAPIScriptable::Allocate,
  NPAPIScriptable::Deallocate,
  NULL,
  NPAPIScriptable::HasMethod,
  NPAPIScriptable::Invoke,
  NPAPIScriptable::InvokeDefault,
  NPAPIScriptable::HasProperty,
  NPAPIScriptable::GetProperty,
  NPAPIScriptable::SetProperty,
  NPAPIScriptable::RemoveProperty,
};

NPAPIPlugin::NPAPIPlugin(CoreObject *core,NPP npp)
:JSApiBase(core),_npp(npp)
{
	_scriptable = (NPAPIScriptable*)npnfuncs->createobject(_npp,&plugin_ref_obj);
//	npnfuncs->retainobject((NPObject*)_scriptable);
}

NPAPIPlugin::~NPAPIPlugin()
{
	npnfuncs->releaseobject((NPObject *)_scriptable);
}

NPAPIScriptable* NPAPIPlugin::GetNPAPIScriptable()
{
	return _scriptable;
}

void NPAPIPlugin::RegisterJSApi()
{
	RegisterJSProperty("test");
}