//
//  PluginInstance.h
//  EzPlugin
//
//  Created by Oleg on 19.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __EzPlugin__PluginInstance__
#define __EzPlugin__PluginInstance__

#include "npapi/npapi.h"
#include "npapi/npfunctions.h"
#import <Cocoa/Cocoa.h>
#include "../VideoPlayer/src/VideoPlayer.h"

namespace PluginLib
{
    class PluginInstance
    {
    private:
        static NPObject*    AllocNPObject(NPP npp, NPClass *aClass);
        static bool         ConstructNPObject    (NPObject *npobj,const NPVariant *args,uint32_t argCount,NPVariant *result);
        static void         DeallocNPObject(NPObject *npobj);
        static bool         EnumerateNPObject(NPObject *npobj, NPIdentifier **value, uint32_t *count);
        static bool         GetPropertyNPObject(NPObject *npobj, NPIdentifier name, NPVariant *result);
        static bool         HasMethodNPObject(NPObject *npobj, NPIdentifier name);
        static bool         HasPropertyNPObject(NPObject *npobj, NPIdentifier name);
        static void         InvalidateNPObject(NPObject *npobj);
        static bool         InvokeNPObject(NPObject *npobj, NPIdentifier name, const NPVariant *args,uint32_t argCount,NPVariant *result);
        static bool         InvokeDefaultNPObject(NPObject *npobj,const NPVariant *args,uint32_t argCount,NPVariant *result);
        static bool         RemovePropertyNPObject(NPObject *npobj,NPIdentifier name);
        static bool         SetPropertyNPObject(NPObject *npobj, NPIdentifier name,const NPVariant *value);
    protected:
        CoreObjectLib::CoreObject *_core;        
        NPNetscapeFuncs     *_browser_funcs;
        NPP                 _npp;
        NPWindow            _window;
        NPClass              _class_func;
        CALayer             *_layer;
        NPVariant           _disc_obj,_peer_con_obj,_peer_disc_obj,_stat_obj,_tracker_stat_obj,_tracker_connect_proc;
                        
        CoreObjectLib::Event0                           OnClick;
        CoreObjectLib::Event2<const std::string &,NPVariant *>             OnPlayStream;
    public:
        PluginInstance(NPNetscapeFuncs *browser_funcs,NPP npp);
        virtual ~PluginInstance();
        
        void ProcessCocoaEvent(const NPCocoaEvent *event);
        CALayer* GetLayer();
        NPClass* GetNPClass();
    };
}

#endif /* defined(__EzPlugin__PluginInstance__) */
