#include "BasicPlugin.h"

static NPNetscapeFuncs* browser;

NPError NP_Initialize(NPNetscapeFuncs* browserFuncs)
{
  /* Save the browser function table. */
    browser = browserFuncs;
    PluginProcessInit();
    
    fprintf(stderr, "GLOBAL INIT\n");

    return NPERR_NO_ERROR;
}


NPError NP_GetEntryPoints(NPPluginFuncs* pluginFuncs)
{
  if (pluginFuncs->size < (offsetof(NPPluginFuncs, setvalue) + sizeof(void*)))
    return NPERR_INVALID_FUNCTABLE_ERROR;

  pluginFuncs->newp = NPP_New;
  pluginFuncs->destroy = NPP_Destroy;
  pluginFuncs->setwindow = NPP_SetWindow;
  pluginFuncs->newstream = NPP_NewStream;
  pluginFuncs->destroystream = NPP_DestroyStream;
  pluginFuncs->asfile = NPP_StreamAsFile;
  pluginFuncs->writeready = NPP_WriteReady;
  pluginFuncs->write = (NPP_WriteProcPtr)NPP_Write;
  pluginFuncs->print = NPP_Print;
  pluginFuncs->event = NPP_HandleEvent;
  pluginFuncs->urlnotify = NPP_URLNotify;
  pluginFuncs->getvalue = NPP_GetValue;
  pluginFuncs->setvalue = NPP_SetValue;

  return NPERR_NO_ERROR;
}

void NP_Shutdown(void)
{
    PluginProcessFree();
    fprintf(stderr, "GLOBAL FREE\n");
}

NPError NPP_New(NPMIMEType pluginType, NPP instance, uint16_t mode, int16_t argc, char* argn[], char* argv[], NPSavedData* saved)
{
    void* plugin_instance;
    NPBool supports_coreanimation   = false;
    NPBool supports_cocoa           = false;
    
    fprintf(stderr, "INIT\n");
    
    if (browser->getvalue(instance, NPNVsupportsCoreAnimationBool, &supports_coreanimation) == NPERR_NO_ERROR && supports_coreanimation)
        browser->setvalue(instance, NPPVpluginDrawingModel, (void*)NPDrawingModelCoreAnimation);
    else
        return NPERR_INCOMPATIBLE_VERSION_ERROR;
    
    if (browser->getvalue(instance, NPNVsupportsCocoaBool, &supports_cocoa) == NPERR_NO_ERROR && supports_cocoa)
        browser->setvalue(instance, NPPVpluginEventModel, (void*)NPEventModelCocoa);
    else
        return NPERR_INCOMPATIBLE_VERSION_ERROR;
    
    plugin_instance = CreatePluginInstance(browser, instance);
    instance->pdata = plugin_instance;
    
    return NPERR_NO_ERROR;
}

/* Called to destroy an instance of the plugin. */
NPError NPP_Destroy(NPP instance, NPSavedData** save)
{
    fprintf(stderr, "FREE\n");    
    DeletePluginInstance(instance->pdata);
    return NPERR_NO_ERROR;
}

/* Called to update a plugin instances's NPWindow. */
NPError NPP_SetWindow(NPP instance, NPWindow* window)
{
/*  PluginInstance* currentInstance = (PluginInstance*)(instance->pdata);

  currentInstance->window = *window;*/
  
  return NPERR_NO_ERROR;
}

NPError NPP_NewStream(NPP instance, NPMIMEType type, NPStream* stream, NPBool seekable, uint16_t* stype)
{
  *stype = NP_ASFILEONLY;
  return NPERR_NO_ERROR;
}

NPError NPP_DestroyStream(NPP instance, NPStream* stream, NPReason reason)
{
  return NPERR_NO_ERROR;
}

int32_t NPP_WriteReady(NPP instance, NPStream* stream)
{
  return 0;
}

int32_t NPP_Write(NPP instance, NPStream* stream, int32_t offset, int32_t len, void* buffer)
{
  return 0;
}

void NPP_StreamAsFile(NPP instance, NPStream* stream, const char* fname)
{
}

void NPP_Print(NPP instance, NPPrint* platformPrint)
{
  
}

int16_t NPP_HandleEvent(NPP instance, void* event)
{
  NPCocoaEvent* cocoaEvent = (NPCocoaEvent*)event;
  if (cocoaEvent)
  {
      ProcessCocoaEvent(instance->pdata, event);
      return 1;
  }

  return 0;
}

void NPP_URLNotify(NPP instance, const char* url, NPReason reason, void* notifyData)
{

}

void AlertString(const char *string)
{
    fprintf(stderr,"%s\n",string);
/*
    DialogRef theItem;
    DialogItemIndex itemIndex;
    CFStringRef result;
    
    
    result = CFStringCreateWithCString (kCFAllocatorDefault ,
                                        string,
                                        kCFStringEncodingMacRoman
                                        );
    
    CreateStandardAlert(kAlertNoteAlert, CFSTR("aaa"), result, NULL, &theItem);
    RunStandardAlert(theItem, NULL, &itemIndex);*/
}


static bool HasProperty(NPObject *npobj, NPIdentifier name)
{
    NPUTF8 *string;

    string = browser->utf8fromidentifier(name);

    AlertString(string);
    return false;
}

static bool HasMethod(NPObject *npobj, NPIdentifier name)
{
    AlertString("has method");
    return false;
}

static bool GetProperty(NPObject *npobj, NPIdentifier name,
                                         NPVariant *result)
{
    
    AlertString("get property");
    return false;
}

static bool SetProperty(NPObject *npobj, NPIdentifier name,
                                const NPVariant *value)
{
    return false;
}

static bool RemoveProperty(NPObject *npobj,NPIdentifier name)
{
    return false;
}

NPError NPP_GetValue(NPP instance, NPPVariable variable, void *value)
{
    NPError ret_val;
    void *plugin_instance;
    NPObject *object;
    
    ret_val = NPERR_NO_ERROR;
    plugin_instance = instance->pdata;
    
//    fprintf(stderr,"GET VALUE :%d\n",variable);
    
    switch (variable)
    {
        case NPPVpluginCoreAnimationLayer:
            *(void**)value = GetCALayer(instance->pdata);
        break;
        case NPPVpluginScriptableNPObject:
            object = browser->createobject(instance,GetNPClass(plugin_instance));
            *(NPObject**)value = object;
        break;
        default:
            ret_val = NPERR_GENERIC_ERROR;
        break;
    }
    
        
/*    PluginInstance *plugin;
    NPObject *object;
    
    plugin = (PluginInstance*)instance->pdata;
    if (variable == NPPVpluginScriptableNPObject)
    {
        plugin->function_class.allocate = NULL;
        plugin->function_class.deallocate = NULL;
        plugin->function_class.construct = NULL;
        plugin->function_class.enumerate = NULL;
        plugin->function_class.hasProperty = HasProperty;
        plugin->function_class.hasMethod = HasMethod;
        plugin->function_class.getProperty = GetProperty;
        plugin->function_class.setProperty = SetProperty;
        plugin->function_class.removeProperty = RemoveProperty;
        plugin->function_class.structVersion = NP_CLASS_STRUCT_VERSION;
        
        object = browser->createobject(instance, &plugin->function_class);
        *(NPObject**)value = object;
        return  NPERR_NO_ERROR;
    }*/
    
    
  return ret_val;
}

NPError NPP_SetValue(NPP instance, NPNVariable variable, void *value)
{
    
//        fprintf(stderr,"SET VALUE :%d",variable);
  return NPERR_GENERIC_ERROR;
}

