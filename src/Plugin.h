#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include "npapi/npapi.h"
#include "npapi/npruntime.h"
#include "npapi/npfunctions.h"
#include "../VideoPlayer/src/VideoPlayer.h"

extern NPNetscapeFuncs* npnfuncs;

namespace PluginLib
{


class Plugin
:public VideoPlayerLib::VideoPlayer
{
public:
  Plugin(NPP pNPInstance);
  ~Plugin();

  NPBool init(NPWindow* pNPWindow);
  NPBool isInitialized();
  ScriptablePluginObject *GetScriptableObject();
#ifdef _WINDOWS
  HWND GetHWnd(); 
#endif
private:
  NPP m_pNPInstance;
  NPWindow * m_Window;
  NPBool m_bInitialized;
  ScriptablePluginObject *m_pScriptableObject;
#ifdef _WINDOWS
  HWND m_hWnd; 
#endif	
  friend class ScriptablePluginObject;
};

}
#endif // __PLUGIN_H__
