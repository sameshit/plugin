#ifndef NPAPISCRIPTABLE___H
#define NPAPISCRIPTABLE___H

#include "npapi/npapi.h"
#include "npapi/npruntime.h"
#include "npapi/npfunctions.h"
#include "JSApiBase.h"

extern NPNetscapeFuncs* npnfuncs;

namespace PluginLib
{
	class NPAPIScriptable: NPObject
		{
		public:
			NPAPIScriptable(NPP instance);
			virtual ~NPAPIScriptable();
		
			static NPObject* Allocate(NPP instance, NPClass* npclass);
			static void Deallocate(NPObject* obj);
			static bool HasMethod(NPObject* obj, NPIdentifier methodName);
			static bool InvokeDefault(NPObject* obj, const NPVariant* args,
								uint32_t argCount, NPVariant* result);
			static bool Invoke(NPObject* obj, NPIdentifier methodName,
							const NPVariant* args, uint32_t argCount,
							NPVariant* result);
			static bool HasProperty(NPObject* obj, NPIdentifier np_name);
			static bool GetProperty(NPObject* obj, NPIdentifier np_name,
									NPVariant* result);
			static bool SetProperty(NPObject *obj, NPIdentifier np_name, const NPVariant *value);
			static bool RemoveProperty(NPObject *obj, NPIdentifier np_name);
			NPP GetNPP() {return _npp;}
		private:
			NPP _npp;
			CoreObjectLib::CoreObject *_core;
			JSApiBase *_plugin;
		};	
}

#endif