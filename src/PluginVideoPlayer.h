//
//  PluginVideoPlayer.h
//  EzPlugin
//
//  Created by Oleg on 20.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __EzPlugin__PluginVideoPlayer__
#define __EzPlugin__PluginVideoPlayer__

#include "PluginInstance.h"

namespace PluginLib
{
    struct PluginArgs
    {
        NPVariant argv[10];
    };
    
    class PluginVideoPlayer
    :public PluginInstance
    {
    protected:
        VideoPlayerLib::VideoPlayer *_video_player;
    private:
        CoreObjectLib::SpinLock _q_lock;
        std::string _stream_name;
        
        void ProcessClick();
        void ProcessPlayStream(const std::string &stream_name,NPVariant *result);
        void ProcessTrackerConnect(MProtoLib::MSession *session);
        void ProcessTrackerDisconnect(MProtoLib::MSession *session);
        void ProcessPeerConnect(MProtoLib::MSession *session);
        void ProcessPeerDisconnect(MProtoLib::MSession *session);
        void ProcessStatistic();
        void ProcessTrackerStatistic(const uint64_t& peer_count,const uint64_t& streamer_download,
                                     const uint64_t& peer_download,const uint64_t &eco);
        
        std::queue<PluginArgs>  _peer_connect_args,
                                _peer_disconnect_args,_statistic_args,_tracker_statistic_args;
    public:
        PluginVideoPlayer(NPNetscapeFuncs *browser_funcs,NPP npp);
        virtual ~PluginVideoPlayer();
    };
}

#endif /* defined(__EzPlugin__PluginVideoPlayer__) */
