//
//  PluginVideoPlayer.cpp
//  EzPlugin
//
//  Created by Oleg on 20.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "PluginVideoPlayer.h"

using namespace VideoPlayerLib;
using namespace PluginLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace PeerLib;

const char*     kTrackerIP = "192.168.1.5";
const uint16_t  kTrackerPort = 12345;
const char*     kStun2IP   = "192.168.1.66";
const uint16_t  kStun1Port = 12466;
const uint16_t  kStun2Port = 12344;



PluginVideoPlayer::PluginVideoPlayer(NPNetscapeFuncs *browser_funcs,NPP npp)
:PluginInstance(browser_funcs,npp)
{
    fast_new(_video_player,_core,_layer);
    
    OnClick.Attach(this,&PluginVideoPlayer::ProcessClick);
    OnPlayStream.Attach(this,&PluginVideoPlayer::ProcessPlayStream);
    
    _video_player->OnTrackerConnect.Attach(this,&PluginVideoPlayer::ProcessTrackerConnect);
    _video_player->OnTrackerDisconnect.Attach(this,&PluginVideoPlayer::ProcessTrackerDisconnect);
    _video_player->OnPeerConnect.Attach(this,&PluginVideoPlayer::ProcessPeerConnect);
    _video_player->OnPeerDisconnect.Attach(this,&PluginVideoPlayer::ProcessPeerDisconnect);
    _video_player->OnStatistic.Attach(this,&PluginVideoPlayer::ProcessStatistic);
    _video_player->OnTrackerStatistic.Attach(this,&PluginVideoPlayer::ProcessTrackerStatistic);
    
    int i = 12900;
    while (!_video_player->Bind(i))
        ++i;
}

PluginVideoPlayer::~PluginVideoPlayer()
{
    _video_player->PeerDeleteFromLoop();
    fast_delete(_video_player);
}

void PluginVideoPlayer::ProcessClick()
{
    _video_player->ToggleFullScreen();
}

void PluginVideoPlayer::ProcessPlayStream(const std::string &stream_name, NPVariant *result)
{
    bool ret_val = true;
    
    _stream_name = stream_name;
    if (_video_player->GetTrackerState() == TrackerNotConnected)
    {
        _video_player->ConnectToTracker(kTrackerIP, kTrackerPort, kStun1Port, kStun2IP, kStun2Port);
        return;
    }
    ret_val = _video_player->PlayStream(_stream_name);
    
    result->type = NPVariantType_Bool;
    result->value.boolValue = ret_val;
}

void PluginVideoPlayer::ProcessTrackerConnect(MSession *session)
{
//    LOG "play stream: "<<std::boolalpha<< _video_player->PlayStream(_stream_name) EL;
    _video_player->PlayStream(_stream_name);
    
}

void PluginVideoPlayer::ProcessTrackerDisconnect(MSession *session)
{
    CFRunLoopRef runloop;
    runloop = CFRunLoopGetMain();
    
    CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode,   ^{
        NPVariant result;
        _browser_funcs->invokeDefault(_npp,_disc_obj.value.objectValue,NULL,0,&result);
    });
}

void PluginVideoPlayer::ProcessPeerConnect(MSession *session)
{
    CFRunLoopRef runloop;
    runloop = CFRunLoopGetMain();
    PluginArgs args;
    
    char *outStr = (char*)_browser_funcs->memalloc((uint32_t)strlen(session->GetIP())+1);
    memcpy(outStr, session->GetIP(), strlen(session->GetIP()) + 1);
    
    args.argv[0].type = NPVariantType_String;
    args.argv[0].value.stringValue.UTF8Characters = outStr;
    args.argv[0].value.stringValue.UTF8Length = (uint32_t)strlen(session->GetIP());
    
    args.argv[1].type = NPVariantType_Double;
    args.argv[1].value.doubleValue = session->GetPort();
    
    _q_lock.Lock();
    _peer_connect_args.push(args);
    _q_lock.UnLock();
    
    CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode,   ^{
        NPVariant result;
        PluginArgs pop_args;
        bool empty;
        
        _q_lock.Lock();
        empty = _peer_connect_args.empty();
        if (!empty)
        {
            pop_args = _peer_connect_args.front();
            _peer_connect_args.pop(); 
        }
        _q_lock.UnLock();
        
        if (!empty)
            _browser_funcs->invokeDefault(_npp,_peer_con_obj.value.objectValue,pop_args.argv,2,&result);
        
    });
}

void PluginVideoPlayer::ProcessPeerDisconnect(MSession *session)
{
    CFRunLoopRef runloop;
    runloop = CFRunLoopGetMain();
    PluginArgs args;
    
    char *outStr = (char*)_browser_funcs->memalloc((uint32_t)strlen(session->GetIP())+1);
    memcpy(outStr, session->GetIP(), strlen(session->GetIP()) + 1);
    
    args.argv[0].type = NPVariantType_String;
    args.argv[0].value.stringValue.UTF8Characters = outStr;
    args.argv[0].value.stringValue.UTF8Length = (uint32_t)strlen(session->GetIP());
    
    args.argv[1].type = NPVariantType_Double;
    args.argv[1].value.doubleValue = session->GetPort();
    
    _q_lock.Lock();
    _peer_disconnect_args.push(args);
    _q_lock.UnLock();
    
    CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode,   ^{
        NPVariant result;
        PluginArgs pop_args;
        bool empty;
        
        _q_lock.Lock();
        empty = _peer_disconnect_args.empty();
        if (!empty)
        {
            pop_args = _peer_disconnect_args.front();
            _peer_disconnect_args.pop();
        }
        _q_lock.UnLock();
        
        if (!empty)
            _browser_funcs->invokeDefault(_npp,_peer_disc_obj.value.objectValue,pop_args.argv,2,&result);

    });
}

void PluginVideoPlayer::ProcessStatistic()
{
    CFRunLoopRef runloop;
    runloop = CFRunLoopGetMain();
    PluginArgs args;
    
    args.argv[0].type             = NPVariantType_Int32;
    args.argv[0].value.intValue = (int32_t)(_video_player->GetCurrentPeerDownload()/1024);
    
    args.argv[1].type             = NPVariantType_Int32;
    args.argv[1].value.intValue = (int32_t)(_video_player->GetCurrentStreamerDownload()/1024);
    
    args.argv[2].type             = NPVariantType_Int32;
    args.argv[2].value.intValue = (int32_t)_video_player->GetCurrentEconomy();
    
    args.argv[3].type             = NPVariantType_Int32;
    args.argv[3].value.intValue = (int32_t)(_video_player->GetTotalPeerDownload()/1024);
    
    args.argv[4].type             = NPVariantType_Int32;
    args.argv[4].value.intValue = (int32_t)(_video_player->GetTotalStreamerDownload()/1024);
    
    args.argv[5].type             = NPVariantType_Int32;
    args.argv[5].value.intValue = (int32_t)_video_player->GetTotalEconomy();
    
    _q_lock.Lock();
    _statistic_args.push(args);
    _q_lock.UnLock();
    
    CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode,   ^{
        NPVariant result;
        PluginArgs pop_args;
        bool empty;
        
        _q_lock.Lock();
        empty = _statistic_args.empty();
        if (!empty)
        {
            pop_args = _statistic_args.front();
            _statistic_args.pop();
        }
        _q_lock.UnLock();
        
        if (!empty)
            _browser_funcs->invokeDefault(_npp,_stat_obj.value.objectValue,pop_args.argv,6,&result);
    });
}

void PluginVideoPlayer::ProcessTrackerStatistic(const uint64_t& peer_count,const uint64_t& streamer_download,
                         const uint64_t& peer_download,const uint64_t &eco)
{
    CFRunLoopRef runloop;
    runloop = CFRunLoopGetMain();
    PluginArgs args;
    
    args.argv[0].type             = NPVariantType_Int32;
    args.argv[0].value.intValue = (int32_t)(peer_count);
    
    args.argv[1].type             = NPVariantType_Int32;
    args.argv[1].value.intValue = (int32_t)(streamer_download/1024);
    
    args.argv[2].type             = NPVariantType_Int32;
    args.argv[2].value.intValue = (int32_t)(peer_download/1024);
    
    args.argv[3].type             = NPVariantType_Int32;
    args.argv[3].value.intValue = (int32_t)(eco);
    
    _q_lock.Lock();
    _tracker_statistic_args.push(args);
    _q_lock.UnLock();
    
    CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode,   ^{
        NPVariant result;
        PluginArgs pop_args;
        bool empty;
        
        _q_lock.Lock();
        empty = _tracker_statistic_args.empty();
        if (!empty)
        {
            pop_args = _tracker_statistic_args.front();
            _tracker_statistic_args.pop();
        }
        _q_lock.UnLock();
        
        if (!empty)
            _browser_funcs->invokeDefault(_npp,_tracker_stat_obj.value.objectValue,pop_args.argv,4,&result);
    });
}