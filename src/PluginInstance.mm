//
//  PluginInstance.cpp
//  EzPlugin
//
//  Created by Oleg on 19.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "PluginInstance.h"
#include <stdint.h>
#include <iostream>

using namespace CoreObjectLib;
using namespace VideoPlayerLib;
using namespace PluginLib;
using namespace std;

PluginInstance::PluginInstance(NPNetscapeFuncs *browser_funcs,NPP npp)
:_browser_funcs(browser_funcs),_npp(npp)
{
    _core = new CoreObject;
    _layer = [[CALayer alloc] init];
        
    _class_func.allocate        = PluginInstance::AllocNPObject;
    _class_func.construct       = PluginInstance::ConstructNPObject;
    _class_func.deallocate      = PluginInstance::DeallocNPObject;
    _class_func.enumerate       = PluginInstance::EnumerateNPObject;
    _class_func.getProperty     = PluginInstance::GetPropertyNPObject;
    _class_func.hasMethod       = PluginInstance::HasMethodNPObject;
    _class_func.hasProperty     = PluginInstance::HasPropertyNPObject;
    _class_func.invalidate      = PluginInstance::InvalidateNPObject;
    _class_func.invoke          = PluginInstance::InvokeNPObject;
    _class_func.invokeDefault   = PluginInstance::InvokeDefaultNPObject;
    _class_func.removeProperty  = PluginInstance::RemovePropertyNPObject;
    _class_func.setProperty     = PluginInstance::SetPropertyNPObject;
    _class_func.structVersion   = NP_CLASS_STRUCT_VERSION;
}

PluginInstance::~PluginInstance()
{
    [_layer release];
    delete _core;
}

CALayer* PluginInstance::GetLayer()
{
    return _layer;
}

NPClass* PluginInstance::GetNPClass()
{
    return &_class_func;
}

void PluginInstance::ProcessCocoaEvent(const NPCocoaEvent* event)
{
    if (event->type == NPCocoaEventMouseUp)
        OnClick();
}

NPObject* PluginInstance::AllocNPObject(NPP npp, NPClass *aClass)
{
    uint8_t *ptr;
    ssize_t plugin_instance_ptr = (ssize_t)npp->pdata;
    
    ptr = (uint8_t*)malloc(sizeof(NPObject)+sizeof(ssize_t));
    
    memcpy(ptr+sizeof(NPObject),&plugin_instance_ptr,sizeof(ssize_t));
    
    return (NPObject*)ptr;
}

bool PluginInstance::ConstructNPObject(NPObject *npobj, const NPVariant *args, uint32_t argCount, NPVariant *result){return false;}

void PluginInstance::DeallocNPObject(NPObject *npobj)
{
    free(npobj);
}

bool PluginInstance::EnumerateNPObject(NPObject *npobj, NPIdentifier **value, uint32_t *count){return false;}

bool PluginInstance::GetPropertyNPObject(NPObject *npobj, NPIdentifier name, NPVariant *result)
{
    return false;
}

bool PluginInstance::HasMethodNPObject(NPObject *npobj, NPIdentifier name)
{
    PluginInstance *plugin_instance;
    ssize_t size_ptr;
    uint8_t *ptr;
    NPUTF8 *str;
    bool ret_val;
    
    ptr = (uint8_t *)npobj;
    ptr += sizeof(NPObject);
    
    memcpy(&size_ptr,ptr,sizeof(ssize_t));
    plugin_instance = (PluginInstance *)size_ptr;
    
    ret_val = false;
    if (plugin_instance->_browser_funcs->identifierisstring(name))
    {
        str = plugin_instance->_browser_funcs->utf8fromidentifier(name);
        
        if (strcmp(str,"PlayStream") == 0)
            ret_val = true;
//        cerr <<"identifier is string with name "<<str<<endl;
        plugin_instance->_browser_funcs->memfree(str);
    }
    
    return ret_val;
}

bool PluginInstance::InvokeNPObject(NPObject *npobj, NPIdentifier name, const NPVariant *args, uint32_t argCount, NPVariant *result)
{
    PluginInstance *plugin_instance;
    ssize_t size_ptr;
    uint8_t *ptr;
    NPUTF8 *str;
    bool ret_val;
    string param_str;
    CoreObject *_core;
    
    ptr = (uint8_t *)npobj;
    ptr += sizeof(NPObject);
    
    memcpy(&size_ptr,ptr,sizeof(ssize_t));
    plugin_instance = (PluginInstance *)size_ptr;
    _core = plugin_instance->_core;
    
    ret_val = false;
    if (plugin_instance->_browser_funcs->identifierisstring(name))
    {
        str = plugin_instance->_browser_funcs->utf8fromidentifier(name);
//        LOG "Invoking "<<str EL;
        if (strcmp(str,"PlayStream") == 0)
        {
            if (argCount == 1 && NPVARIANT_IS_STRING(args[0]))
            {
                param_str.assign(args[0].value.stringValue.UTF8Characters,args[0].value.stringValue.UTF8Length);
                plugin_instance->OnPlayStream(param_str,result);
                ret_val = true;
            }
        }
        plugin_instance->_browser_funcs->memfree(str);
    }
    
    return ret_val;
}

bool PluginInstance::HasPropertyNPObject(NPObject *npobj, NPIdentifier name)
{
    PluginInstance *plugin_instance;
    ssize_t size_ptr;
    uint8_t *ptr;
    NPUTF8 *str;
    bool ret_val = false;
    CoreObject *_core;
    
    ptr = (uint8_t *)npobj;
    ptr += sizeof(NPObject);
    
    memcpy(&size_ptr,ptr,sizeof(ssize_t));
    plugin_instance = (PluginInstance *)size_ptr;
    _core = plugin_instance->_core;
    
    if (plugin_instance->_browser_funcs->identifierisstring(name))
    {
        str = plugin_instance->_browser_funcs->utf8fromidentifier(name);
        
//        LOG "has property name: "<<str EL;
        if (strcmp(str,"onTrackerDisconnect") == 0 ||
            strcmp(str,"onPeerConnect") == 0 ||
            strcmp(str,"onPeerDisconnect") == 0 ||
            strcmp(str,"onStatistic") == 0 ||
            strcmp(str,"onTrackerStatistic") == 0 ||
            strcmp(str,"onTrackerConnect") == 0)
            ret_val = true;
        
        plugin_instance->_browser_funcs->memfree(str);
    }
    return ret_val;
}

bool PluginInstance::SetPropertyNPObject(NPObject *npobj, NPIdentifier name, const NPVariant *value)
{
    PluginInstance *plugin_instance;
    ssize_t size_ptr;
    uint8_t *ptr;
    NPUTF8 *str;
    bool ret_val = false;
    CoreObject *_core;
    
    ptr = (uint8_t *)npobj;
    ptr += sizeof(NPObject);
    
    memcpy(&size_ptr,ptr,sizeof(ssize_t));
    plugin_instance = (PluginInstance *)size_ptr;
    _core = plugin_instance->_core;
    
    if (plugin_instance->_browser_funcs->identifierisstring(name))
    {
        str = plugin_instance->_browser_funcs->utf8fromidentifier(name);
        
//        LOG "set property name: "<<str EL;
        if (value->type == NPVariantType_Object)
        {
            if (strcmp(str,"onTrackerDisconnect") == 0)
            {
                ret_val = true;
                plugin_instance->_disc_obj = *value;
                ++plugin_instance->_disc_obj.value.objectValue->referenceCount;
            }
            else if (strcmp(str,"onPeerConnect") == 0)
            {
                ret_val = true;
                plugin_instance->_peer_con_obj = *value;
                ++plugin_instance->_peer_con_obj.value.objectValue->referenceCount;
            }
            else if (strcmp(str,"onPeerDisconnect") == 0)
            {
                ret_val = true;
                plugin_instance->_peer_disc_obj = *value;
                ++plugin_instance->_peer_disc_obj.value.objectValue->referenceCount;
            }
            else if (strcmp(str,"onStatistic") == 0)
            {
                ret_val = true;
                plugin_instance->_stat_obj = *value;
                ++plugin_instance->_stat_obj.value.objectValue->referenceCount;
            }
            else if (strcmp(str,"onTrackerStatistic") == 0)
            {
                ret_val = true;
                plugin_instance->_tracker_stat_obj = *value;
                ++plugin_instance->_tracker_stat_obj.value.objectValue->referenceCount;
            }
        }
        
        plugin_instance->_browser_funcs->memfree(str);
    }
    
    return ret_val;
}
void PluginInstance::InvalidateNPObject(NPObject *npobj) {}
bool PluginInstance::InvokeDefaultNPObject(NPObject *npobj, const NPVariant *args, uint32_t argCount,NPVariant *result) {return false;}
bool PluginInstance::RemovePropertyNPObject(NPObject *npobj, NPIdentifier name) {return false;}


