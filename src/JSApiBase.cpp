#include "JSApiBase.h"

using namespace PluginLib;
using namespace CoreObjectLib;
using namespace std;


JSApiBase::JSApiBase(CoreObject *core)
:VideoPlayer(core)
{

}


JSApiBase::~JSApiBase(void)
{
}

bool JSApiBase::RegisterJSMethod(const char *name,const VariantFunction &function)
{
	RETURN_MSG_IF_FALSE(_methods_by_name.count(name) == 0,"Method '"<<name<<"' already registered");
	_methods_by_name[name] = function;
	return true;
}

bool JSApiBase::UnRegisterJSMethod(const char *name)
{
	RETURN_MSG_IF_TRUE(_methods_by_name.count(name)==0, "Method '"<<name<<"' isn't registered");
	_methods_by_name.erase(name);
	return true;
}

bool JSApiBase::GetMethod(const char *name, VariantFunction *func)
{
	RETURN_IF_FALSE(HasJSMethod(name));

	*func = _methods_by_name[name];
	return true;
}

bool JSApiBase::RegisterJSProperty(const char *name,const Variant &val)
{
	RETURN_MSG_IF_FALSE(_properties_by_name.count(name) == 0,"Property '"<<name<<"' already registered");
	RETURN_MSG_IF_FALSE(_events_by_name.count(name) == 0,"Event '"<<name<<"' already registered, unregister it first");
	
	_properties_by_name[name] = val;
	return true;
}

bool JSApiBase::UnRegisterJSProperty(const char *name)
{
	RETURN_MSG_IF_FALSE(_events_by_name.count(name) == 0, "Event '"<<name<<"' already registered");
	RETURN_MSG_IF_TRUE(_properties_by_name.count(name) == 0, "Property '"<<name<<"' doesn't exist");

	_properties_by_name.erase(name);
	return true;
}

bool JSApiBase::RegisterJSEvent(const char *name)
{
	RETURN_MSG_IF_FALSE(_properties_by_name.count(name) == 0,"Property '"<<name<<"' already registered");
	RETURN_MSG_IF_FALSE(_events_by_name.count(name) == 0,"Event '"<<name<<"' already registered, unregister it first");

	_events_by_name[name] = nullptr;
	return true;
}

bool JSApiBase::UnRegisterJSEvent(const char *name)
{
	RETURN_MSG_IF_FALSE(_events_by_name.count(name) == 0, "Event '"<<name<<"' already registered");
	RETURN_MSG_IF_TRUE(_properties_by_name.count(name) == 0, "Property '"<<name<<"' doesn't exist");

	_events_by_name.erase(name);
	return true;
}

bool JSApiBase::SetJSEvent(const char *name,void *plugin_obj)
{
	RETURN_MSG_IF_FALSE(_events_by_name.count(name) == 0,"Event '"<<name<<"' is not registered");

	_events_by_name[name] = plugin_obj;
	return true;
}

bool JSApiBase::GetJSEvent(const char *name, void **plugin_obj)
{
	RETURN_MSG_IF_FALSE(_events_by_name.count(name) == 0,"Event '"<<name<<"' is not registered");

	*plugin_obj = _events_by_name[name];
	return true;
}

bool JSApiBase::SetJSProperty(const char *name, const Variant &val)
{
	RETURN_MSG_IF_TRUE(_properties_by_name.count(name) == 0,"Property '"<<name<<"' doesn't exist");

	_properties_by_name[name] = val;
	return true;
}

bool JSApiBase::GetJSProperty(const char *name, Variant *val)
{
	RETURN_MSG_IF_TRUE(_properties_by_name.count(name) == 0,"Property '"<<name<<"' doesn't exist");

	*val = _properties_by_name[name];
	return true;
}

bool JSApiBase::HasJSProperty(const char *name)
{
	return _properties_by_name.count(name)==1;
}

bool JSApiBase::HasJSEvent(const char *name)
{
	return _events_by_name.count(name) == 1;
}

bool JSApiBase::HasJSMethod(const char *name)
{
	return _methods_by_name.count(name) == 1;
}