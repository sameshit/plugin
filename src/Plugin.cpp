#include <string.h>
#include "plugin.h"
using namespace PluginLib;
using namespace VideoPlayerLib;
using namespace CoreObjectLib;


Plugin::Plugin(NPP pNPInstance) :VideoPlayer(nullptr,nullptr),
  m_pNPInstance(pNPInstance),
  m_bInitialized(false),
  m_pScriptableObject(NULL) {
#ifdef _WINDOWS
  m_hWnd = NULL;
#endif
}

Plugin::~Plugin() {
  if (m_pScriptableObject)
    npnfuncs->releaseobject((NPObject*)m_pScriptableObject);
#ifdef _WINDOWS
  m_hWnd = NULL;
#endif
  m_bInitialized = false;
}

NPBool Plugin::init(NPWindow* pNPWindow) {
  if(pNPWindow == NULL)
    return false;
#ifdef _WINDOWS
  m_hWnd = (HWND)pNPWindow->window;
  if(m_hWnd == NULL)
    return false;
#endif
  m_Window = pNPWindow;
  m_bInitialized = true;
  return true;
}

NPBool Plugin::isInitialized() {
  return m_bInitialized;
}

ScriptablePluginObject * Plugin::GetScriptableObject() {
  if (!m_pScriptableObject) {
    m_pScriptableObject = (ScriptablePluginObject*)npnfuncs->createobject(m_pNPInstance, &plugin_ref_obj);

    // Retain the object since we keep it in plugin code
    // so that it won't be freed by browser.
    npnfuncs->retainobject((NPObject*)m_pScriptableObject);
  }

  return m_pScriptableObject;
}

bool Plugin::RegisterJSMethod(const char *name, const VariantFunction &func)
{
	RETURN_MSG_IF_FALSE(_methods_by_name.count(name)==0,"Method '"<<name<<"' already exists");
	_methods_by_name[name]=func;
	return true;
}

bool Plugin::UnRegisterJSMethod(const char *name)
{
	RETURN_MSG_IF_TRUE(_methods_by_name.count(name)==0, "Method '"<<name<<"' doesn't exist");
	_methods_by_name.erase(name);
	return true;
}

#ifdef _WINDOWS
HWND Plugin::GetHWnd() {
  return m_hWnd;
}
#endif