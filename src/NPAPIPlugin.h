#ifndef NPAPIPLUGIN_____H
#define NPAPIPLUGIN_____H

#include "NPAPIScriptable.h"

namespace PluginLib
{
	class NPAPIPlugin
	:public JSApiBase
	{
	public:
		NPAPIPlugin(CoreObjectLib::CoreObject *core,NPP npp);
		virtual ~NPAPIPlugin();

		NPAPIScriptable* GetNPAPIScriptable();
		void RegisterJSApi();
	private:
		NPAPIScriptable *_scriptable;
		NPP _npp;
	};
}

#endif