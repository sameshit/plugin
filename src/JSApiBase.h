#ifndef JSApiBase___H
#define JSApiBase___H

#include "../VideoPlayer/src/VideoPlayer.h"

namespace PluginLib
{

typedef std::vector<CoreObjectLib::Variant> VariantVector;
typedef std::function<CoreObjectLib::Variant (const VariantVector &)> VariantFunction;
typedef std::unordered_map<std::string, VariantFunction> JSMethodsMap;
typedef JSMethodsMap::iterator JSMethodsMapIterator;
typedef std::unordered_map<std::string,void*> JSEventMap;
typedef JSEventMap::iterator JSEventMapIterator;
typedef std::unordered_map<std::string,CoreObjectLib::Variant> JSPropertyMap;
typedef JSPropertyMap::iterator JSPropertyMapIterator;

class PluginInfo
	{
	public:
		void						*plugin;
		CoreObjectLib::CoreObject	*core;
	};

class JSApiBase :
public VideoPlayerLib::VideoPlayer
{
public:
	JSApiBase(CoreObjectLib::CoreObject *core);
	virtual ~JSApiBase();

	bool RegisterJSMethod(const char *name, const VariantFunction &function);
	bool UnRegisterJSMethod(const char *name);
	bool GetMethod(const char *name,VariantFunction *function);
	bool RegisterJSEvent(const char *name);
	bool UnRegisterJSEvent(const char *name);
	bool SetJSEvent(const char *name,void *plugin_obj);
	bool GetJSEvent(const char *name, void **plugin_obj);
	bool RegisterJSProperty(const char *name, const CoreObjectLib::Variant &val = CoreObjectLib::Variant());
	bool UnRegisterJSProperty(const char *name);
	bool SetJSProperty(const char *name, const CoreObjectLib::Variant &val);
	bool GetJSProperty(const char *name, CoreObjectLib::Variant *val);
	bool HasJSMethod(const char *name);
	bool HasJSProperty(const char *name);
	bool HasJSEvent(const char *name);

	template<typename T>
	bool RegisterJSMethod(const char *name,T *class_ptr, CoreObjectLib::Variant (T::* fn_ptr)(const VariantVector&))
	{
		bool ret_val;
		VariantFunction func;
		func = std::bind(fn_ptr,class_ptr);
		ret_val = RegisterJSMethod(name,func);
		return ret_val;
	}
private:
	JSMethodsMap _methods_by_name;
	JSEventMap _events_by_name;
	JSPropertyMap _properties_by_name;
};

}
#endif