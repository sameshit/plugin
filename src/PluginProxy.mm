//
//  PluginInstance.cpp
//  EzPlugin
//
//  Created by Oleg on 19.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "PluginVideoPlayer.h"
#include "PluginProxy.h"

using namespace PluginLib;
using namespace CoreObjectLib;

extern "C" void PluginProcessInit()
{
 
}

extern "C" void PluginProcessFree()
{
 
}

extern "C" void* CreatePluginInstance(NPNetscapeFuncs *browser_func,NPP npp)
{
    PluginVideoPlayer *instance;
    instance = new PluginVideoPlayer(browser_func,npp);
    return instance;
}

extern "C" void DeletePluginInstance(void *value)
{
    PluginVideoPlayer *instance;
    instance = (PluginVideoPlayer *)value;
    delete instance;
}

extern "C" void* GetCALayer(void *value)
{
    PluginVideoPlayer *instance;
    instance = (PluginVideoPlayer *)value;
    return instance->GetLayer();
}

extern "C" NPClass* GetNPClass(void *value)
{
    PluginVideoPlayer *instance;
    instance = (PluginVideoPlayer *)value;
    return instance->GetNPClass();
}

extern "C" void ProcessCocoaEvent(void *value,NPCocoaEvent *event)
{
    PluginVideoPlayer *instance;
    instance = (PluginVideoPlayer *)value;
    instance->ProcessCocoaEvent(event);
}