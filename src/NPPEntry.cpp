#include <stdio.h>
#include "NPAPIPlugin.h"

using namespace PluginLib;
using namespace CoreObjectLib;

NPError NPP_GetValue(NPP instance, NPPVariable variable, void* value) 
{
	PluginInfo *info;
	NPAPIPlugin *plugin;

	switch(variable)
	{
		case NPPVpluginNameString:
			*((char **)value) = "JetPeer plugin";
		break;
		case NPPVpluginDescriptionString:
			*((char **)value) = "JetPeer player plugin";
		break;
		case NPPVpluginScriptableNPObject: 
			if(instance == NULL)
				return NPERR_INVALID_INSTANCE_ERROR;
			info = (PluginInfo *)instance->pdata;
			plugin = (NPAPIPlugin*)info->plugin;
			*(NPObject **)value = (NPObject*)plugin->GetNPAPIScriptable();
	
		break;
		case NPPVpluginNeedsXEmbed:
			*((char *)value) = 1;
		break;
		default:
			return NPERR_GENERIC_ERROR;
	}
  return NPERR_NO_ERROR;
}

NPError NPP_New(NPMIMEType pluginType, NPP instance,
                uint16_t mode, int16_t argc, char* argn[],
                char* argv[], NPSavedData* saved) 
{
	PluginInfo *info;
	NPAPIPlugin *plugin;
	CoreObject *_core;

	if(instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	info = new PluginInfo;
	info->core = new CoreObject;
	_core = info->core;
	instance->pdata = (void *)info;
	fast_new(plugin,info->core,instance);
	info->plugin = plugin;

#if defined(OS_WIN)
	int windowed = 1;
	npnfuncs->setvalue(instance, NPPVpluginWindowBool, (void *)windowed);
#endif

	return NPERR_NO_ERROR;
}

NPError NPP_Destroy(NPP instance, NPSavedData** save) 
{
	PluginInfo *info;
	NPAPIPlugin *plugin;
	CoreObject *_core;

	if(instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	info = (PluginInfo *)instance->pdata;
	_core = info->core;
	plugin = (NPAPIPlugin*)info->plugin;
	fast_delete(plugin);
	delete info->core;
	delete info;

	return NPERR_NO_ERROR;
}

NPError NPP_SetWindow(NPP instance, NPWindow* window)
{
#if defined(OS_WIN)
	NPAPIPlugin *plugin;
	PluginInfo *info;
	if(instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	if(window == NULL)
		return NPERR_GENERIC_ERROR;

	info = (PluginInfo *)instance->pdata;
	plugin = (NPAPIPlugin*)info->plugin;
	if(plugin == NULL) 
		return NPERR_GENERIC_ERROR;
	plugin->SetDrawingSurface((HWND)window->window);
	plugin->RegisterJSApi();
#endif
  return NPERR_NO_ERROR;
}

NPError NPP_NewStream(NPP instance, NPMIMEType type, NPStream* stream,
                      NPBool seekable, uint16_t* stype) 
{

  return NPERR_GENERIC_ERROR;
}

NPError NPP_DestroyStream(NPP instance, NPStream* stream, NPReason reason) 
{
  return NPERR_GENERIC_ERROR;
}

int16_t NPP_HandleEvent(NPP instance, void* event) 
{
  return 0;
}

